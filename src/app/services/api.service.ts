import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { getUsers, login, meData } from '../operations/query';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { registerData } from '../operations/mutation';
import { RegisterData } from '../components/registro-login/registro-principal/register.interface';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private apollo: Apollo) {}

  // Lista de usuarios
  getUsers() {
    return this.apollo
      .watchQuery({
        query: getUsers,
        fetchPolicy: 'network-only',
      })
      .valueChanges.pipe(
        map((result: any) => {
          return result.data.users;
        })
      );
  }

  //Login
  login(email: string, password: string) {
    return this.apollo
      .watchQuery({
        query: login,
        variables: {
          email: email,
          password: password,
        },
        fetchPolicy: 'network-only',
      })
      .valueChanges.pipe(
        map((result: any) => {
          return result.data.login;
        })
      );
  }

  register(user: RegisterData) {
    return this.apollo
      .mutate({
        mutation: registerData,
        variables: {
          user
        }
      });
  }

  
}
