import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import {
  meData,
  login,
  gethistclinico,
  getTratamientos,
  getIdPaciente,
  getCitaId,
  getHistorialDen,
} from '../operations/query';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs/internal/Subject';
import { MeData } from '../components/main-user/user.interface';
import { Router } from '@angular/router';
import { setCita } from '../operations/mutation';
import { InputCita } from '../components/agenda-cita/agendarCita.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public accessVar = new Subject<boolean>();
  public accessVar$ = this.accessVar.asObservable();
  public userVar = new Subject<MeData>();
  public userVar$ = this.userVar.asObservable();

  constructor(private apollo: Apollo, private router: Router) {}

  public updateStateSession(newValue: boolean) {
    this.accessVar.next(newValue);
  }

  public updateUser(newValue: MeData) {
    this.userVar.next(newValue);
  }

  logout() {
    this.updateStateSession(false);
    localStorage.clear()
    const currentRouter = this.router.url;
    if (currentRouter !== '/register' && currentRouter !== '/users') {
      this.router.navigate(['/login']);
    }
  }

  private sincroValues(result: MeData, state: boolean) {
    this.updateStateSession(state);
    this.updateUser(result);
  }
  start() {
    if (localStorage.getItem('token') !== null) {
      this.getMe().subscribe((result: MeData) => {
        // console.log('start', result );

        if (result.status) {
          localStorage.setItem('id_user', result.user.id_usuario);
          this.getIdPaciente(Number(localStorage.getItem('id_user'))).subscribe(
            (res) => {
              console.log(res.getPacienteid.id_paciente);
              localStorage.setItem(
                'id_paciente',
                res.getPacienteid.id_paciente
              );
            }
          );
          if (this.router.url === '/login') {
            this.sincroValues(result, true);
            this.router.navigate(['/main-user']);
          }
        }
        this.sincroValues(result, result.status);
      });
    } else {
      // No hay token
      this.sincroValues(null, false);
    }
  }

  // get PersonalInfo
  getMe() {
    return this.apollo
      .watchQuery({
        query: meData,
        fetchPolicy: 'network-only',
        context: {
          headers: new HttpHeaders({
            authorization: localStorage.getItem('token'),
          }),
        },
      })
      .valueChanges.pipe(
        map((result: any) => {
          console.log('Esto regresa', result.data.user);

          return result.data.me;
        })
      );
  }

  //Login
  login(email: string, password: string) {
    return this.apollo
      .watchQuery({
        query: login,
        variables: {
          email: email,
          password: password,
        },
        fetchPolicy: 'network-only',
      })
      .valueChanges.pipe(
        map((result: any) => {
          return result.data.login;
        })
      );
  }

  getIdPaciente(id_usuario: number) {
    return this.apollo
      .watchQuery({
        query: getIdPaciente,
        variables: {
          id_usuario: id_usuario,
        },
        fetchPolicy: 'network-only',
      })
      .valueChanges.pipe(
        map((result: any) => {
          return result.data;
        })
      );
  }

  getHisClinico(id_paciente: number, token: string) {
    return this.apollo
      .watchQuery({
        query: gethistclinico,
        variables: {
          id_paciente: id_paciente,
          token: token,
        },
        fetchPolicy: 'network-only',
      })
      .valueChanges.pipe(
        map((result: any) => {
          return result.data;
        })
      );
  }

  getTratamientos() {
    return this.apollo
      .watchQuery({
        query: getTratamientos,
        fetchPolicy: 'network-only',
      })
      .valueChanges.pipe(
        map((result: any) => {
          return result.data;
        })
      );
  }

  setCita(cita: InputCita) {
    return this.apollo.mutate({
      mutation: setCita,
      variables: {
        cita,
      },
    });
  }

  getCita(idPacientes: number) {
    return this.apollo
      .watchQuery({
        query: getCitaId,
        variables: {
          idPacientes,
        },
        fetchPolicy: 'network-only',
      })
      .valueChanges.pipe(
        map((result: any) => {
          return result.data;
        })
      );
  }

  getHistDentista() {
    return this.apollo
      .watchQuery({
        query: getHistorialDen,
        fetchPolicy: 'network-only',
      })
      .valueChanges.pipe(
        map((result: any) => {
          return result.data;
        })
      );
  }
}
