import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageErrorComponent } from './components/page-error/page-error.component';
import { IndexComponent } from './components/index/index.component';
import { RegistroPrincipalComponent } from './components/registro-login/registro-principal/registro-principal.component';
import { RegistroLoginComponent } from './components/registro-login/login-dentista/registro-login.component';
import { MainUserComponent } from './components/main-user/main-user.component';
import { GraphqlModule } from './graphql/graphql.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AgendaCitaComponent } from './components/agenda-cita/agenda-cita.component';
import { BuscarHorarioComponent } from './components/buscar-horario/buscar-horario.component';
import { HistorialUsuarioComponent } from './components/historial-usuario/historial-usuario.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginUserComponent } from './components/registro-login/login-user/login-user.component';
import { SepareteLoginsComponent } from './components/registro-login/separete-logins/separete-logins.component';
import { MainDentistaComponent } from './components/dentista/main-dentista/main-dentista.component';
import { HistorialDentistaComponent } from './components/dentista/historial-dentista/historial-dentista.component';
import { HistCliuserComponent } from './components/hist-cliuser/hist-cliuser.component';

@NgModule({
  declarations: [
    AppComponent,
    PageErrorComponent,
    IndexComponent,
    RegistroPrincipalComponent,
    RegistroLoginComponent,
    MainUserComponent,
    AgendaCitaComponent,
    BuscarHorarioComponent,
    HistorialUsuarioComponent,
    NavbarComponent,
    LoginUserComponent,
    SepareteLoginsComponent,
    MainDentistaComponent,
    HistorialDentistaComponent,
    HistCliuserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GraphqlModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
