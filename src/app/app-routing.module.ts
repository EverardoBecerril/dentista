import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageErrorComponent } from './components/page-error/page-error.component';
import { IndexComponent } from "./components/index/index.component";
import { RegistroPrincipalComponent } from "./components/registro-login/registro-principal/registro-principal.component";
import { MainUserComponent } from "./components/main-user/main-user.component";
import { AgendaCitaComponent } from "./components/agenda-cita/agenda-cita.component";
import { BuscarHorarioComponent } from "./components/buscar-horario/buscar-horario.component";
import { HistorialUsuarioComponent } from "./components/historial-usuario/historial-usuario.component";
import { RegistroLoginComponent } from './components/registro-login/login-dentista/registro-login.component';
import { LoginUserComponent } from './components/registro-login/login-user/login-user.component';
import { SepareteLoginsComponent } from './components/registro-login/separete-logins/separete-logins.component';
import { AuthGuardsService } from './guards/auth-guards.service';
import { MainDentistaComponent } from './components/dentista/main-dentista/main-dentista.component';
import { HistorialDentistaComponent } from './components/dentista/historial-dentista/historial-dentista.component';
import { HistCliuserComponent } from './components/hist-cliuser/hist-cliuser.component';


const routes: Routes = [
{path: '', component: IndexComponent},
{path: 'login', component: RegistroLoginComponent},
{path: 'registro', component: RegistroPrincipalComponent},
{path: 'main-user', component: MainUserComponent, canActivate: [AuthGuardsService]},
{path: 'agenda-cita', component: AgendaCitaComponent, canActivate: [AuthGuardsService]},
{path: 'buscar-horario', component: BuscarHorarioComponent, canActivate: [AuthGuardsService]},
{path: 'historial-usuario', component: HistorialUsuarioComponent, canActivate: [AuthGuardsService]},
{path: 'main-dentista', component: MainDentistaComponent, canActivate: [AuthGuardsService]},
{path: 'historial-dentista', component: HistorialDentistaComponent, canActivate: [AuthGuardsService]},
{path: 'historialclinico-usuario', component: HistCliuserComponent, canActivate: [AuthGuardsService]},


{path: 'error404', component: PageErrorComponent},
{path: '**', component: PageErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
