import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-hist-cliuser',
  templateUrl: './hist-cliuser.component.html',
  styleUrls: ['./hist-cliuser.component.css'],
})
export class HistCliuserComponent implements OnInit {
  id_paciente: number;
  token: string;
  status: boolean;
  enfermedad: String;

  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.id_paciente = Number(localStorage.getItem('id_user'));
    this.token = localStorage.getItem('token');
    this.auth.getHisClinico(this.id_paciente, this.token).subscribe((res) => {
      this.status = res.gethistclinico.status;
      console.log(this.id_paciente);
      
      if (this.status === false) {
        Swal.fire({
          title: 'WOOO!',
          text: 'Por el momento aún no tiene un Historial Clinico',
          icon: 'info',
          confirmButtonText: 'Accept',
        }).then(() => {
          this.router.navigate(['/main-user']);
        });
      } else {
        localStorage.setItem('id_paciente', res.gethistclinico.id_paciente);
        this.enfermedad = res.gethistclinico.histoClini.enfermedades.trim().normalize();
        console.log(this.enfermedad);
      }
    });
  }
}
