import { Component, OnInit } from '@angular/core';
import { LoginData, LoginResult } from './login.interface';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MeData } from '../../main-user/user.interface';
@Component({
  selector: 'app-registro-login',
  templateUrl: './registro-login.component.html',
  styleUrls: ['./registro-login.component.css'],
})
export class RegistroLoginComponent implements OnInit {
  user: LoginData = {
    email: '',
    password: '',
  };
  show: boolean;
  constructor(private auth: AuthService, private router: Router) {
    this.auth.userVar$.subscribe((data: MeData) => {
      if (data === null || data.status === false) {
        this.show = true;
      } else {
        this.show = false;
      }
    });
  }

  ngOnInit(): void {}

  save() {
    // console.log(this.user);
    this.auth
      .login(this.user.email, this.user.password)
      .subscribe((result: LoginResult) => {
        if (result.status) {
          // console.log(result);
          localStorage.setItem('token', result.token);
          this.auth.getMe().subscribe((result)=>{
            // console.log(result.user.id_tipo_usuario);
            localStorage.setItem('id_tipo_usuario',result.user.id_tipo_usuario);
          })
          this.auth.updateStateSession(true);
          this.router.navigate(['/main-user']);
        } else {
          localStorage.removeItem('token');
          this.auth.updateStateSession(false);
        }
      });
  }
}
