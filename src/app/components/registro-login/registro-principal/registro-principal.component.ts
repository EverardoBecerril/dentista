import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { RegisterData, RegisterResult } from './register.interface';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro-principal',
  templateUrl: './registro-principal.component.html',
  styleUrls: ['./registro-principal.component.css'],
})
export class RegistroPrincipalComponent implements OnInit {
  operator: number;
  message: string;
  registerUser: RegisterData = {
    nombre_usuario: '',
    apellido_usuario: '',
    telefono: '',
    fecha_nacimiento: null,
    genero: 0,
    id_tipo_usuario: 0,
    email: '',
    password: '',
  };
  constructor(private auth: AuthService, private api: ApiService, private router: Router) {}

  ngOnInit(): void {}

  save() {
    this.registerUser.id_tipo_usuario = Number(this.registerUser.id_tipo_usuario);
    this.registerUser.genero = Number(this.registerUser.genero); 
    // console.log(this.registerUser);
    
    this.api.register(this.registerUser).subscribe(({data})=>{
      // console.log(data);
      const userResult: any = data;
      // console.log(userResult.registerUser.user);
      if (userResult.registerUser.status) {
        this.operator = 1;
        Swal.fire({
          title: 'Genial!',
          text: 'Ya tiene su cuenta',
          icon: 'success',
          confirmButtonText: 'Accept',
        }).then(()=>{
          this.router.navigate(['/login']);
        });
      } else {
        this.operator = 2;
      }
      this.message = userResult.registerUser.message;
         
    }, (error) => {
      console.log('error enviando el query: ', error);
      this.operator = 3;
      this.message = 'Error inesperado';
    })
   
  }
}
