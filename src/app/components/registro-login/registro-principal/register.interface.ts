export interface RegisterData {
  nombre_usuario: string;
  apellido_usuario: string;
  telefono: string;
  fecha_nacimiento: Date;
  genero: number;
  id_tipo_usuario: number;
  email: string;
  password: string;
}

export interface RegisterResult {
  status: boolean;
  message: string;
  user?: User;
}

interface User {
  id_usuario: number;
  nombre_usuario: string;
  email: string;
  id_tipo_usuario: number;
}
