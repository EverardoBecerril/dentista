import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-historial-usuario',
  templateUrl: './historial-usuario.component.html',
  styleUrls: ['./historial-usuario.component.css']
})
export class HistorialUsuarioComponent implements OnInit {
citas:any
  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.getCita(Number(localStorage.getItem('id_paciente'))).subscribe((result:any)=>{
      this.citas = result.getCitaId;
      console.log(this.citas);
    })
  }

}
