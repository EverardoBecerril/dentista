import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-historial-dentista',
  templateUrl: './historial-dentista.component.html',
  styleUrls: ['./historial-dentista.component.css']
})
export class HistorialDentistaComponent implements OnInit {
  resul:any;
  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.getHistDentista().subscribe((res:any)=>{
      console.log(res.getHistorialDen);
      this.resul = res.getHistorialDen;
    })
  }

}
