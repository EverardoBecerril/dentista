import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MeData } from  '../../main-user/user.interface'

@Component({
  selector: 'app-main-dentista',
  templateUrl: './main-dentista.component.html',
  styleUrls: ['./main-dentista.component.css']
})
export class MainDentistaComponent implements OnInit {
  user: any;
  constructor(
    private api: ApiService,
    private router: Router,
    private auth: AuthService
  ) {
    this.auth.userVar$.subscribe((data: MeData) => {
      if (data !== null && data !== undefined) {
        this.user = data.user;
      }
    });
  }

  ngOnInit(): void {
  }

  logout() {
    this.auth.logout();
  }
}
