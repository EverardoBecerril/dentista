import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { InputCita } from './agendarCita.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agenda-cita',
  templateUrl: './agenda-cita.component.html',
  styleUrls: ['./agenda-cita.component.css'],
})
export class AgendaCitaComponent implements OnInit {
  tratamiento: any;

  agendarCita: InputCita = {
    idPacientes: Number(localStorage.getItem('id_paciente')),
    Tratamiento_idTratamiento: null,
    fechaUsuario_cita: null,
  };

  constructor(private router: Router, private auth: AuthService) {}

  ngOnInit(): void {
    this.auth.getTratamientos().subscribe((resul: any) => {
      this.tratamiento = resul.getTratamineto;
      console.log(this.tratamiento);
    });
  }

  save() {
    this.agendarCita.Tratamiento_idTratamiento = Number(
      this.agendarCita.Tratamiento_idTratamiento
    );
    console.log(this.agendarCita);
    this.auth.setCita(this.agendarCita).subscribe((result: any) => {
      if (result.data.setCita.status) {
        Swal.fire({
          title: 'Genial!',
          text: 'Ha agendado su cita',
          icon: 'success',
          confirmButtonText: 'Accept',
        }).then(()=>{
          this.router.navigate(['/main-user']);
        });
      }
    });
  }
}