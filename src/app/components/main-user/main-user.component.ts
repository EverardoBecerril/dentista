import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MeData } from './user.interface';

@Component({
  selector: 'app-main-user',
  templateUrl: './main-user.component.html',
  styleUrls: ['./main-user.component.css'],
})
export class MainUserComponent implements OnInit {
  user: any;
  view: number;
  constructor(
    private api: ApiService,
    private router: Router,
    private auth: AuthService
  ) {
    this.auth.userVar$.subscribe((data: MeData) => {
      if (data !== null && data !== undefined) {
        this.user = data.user;
      }
      this.view = Number(localStorage.getItem('id_tipo_usuario'));
    });
  }

  ngOnInit(): void {
    // this.view = Number(localStorage.getItem('id_tipo_usuario'));
    this.auth.start();
  }

  logout() {
    this.auth.logout();
  }
}
