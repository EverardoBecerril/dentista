import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarHorarioComponent } from './buscar-horario.component';

describe('BuscarHorarioComponent', () => {
  let component: BuscarHorarioComponent;
  let fixture: ComponentFixture<BuscarHorarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscarHorarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscarHorarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
