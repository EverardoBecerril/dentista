import gql from 'graphql-tag';

export const getUsers = gql`
  query {
    users {
      id
      name
      lastname
      email
      registerDate
    }
  }
`;

export const meData = gql`
  query {
    me {
      status
      message
      user {
        id_usuario
        nombre_usuario
        apellido_usuario
        telefono
        email
        fecha_nacimiento
        id_tipo_usuario
        registerdate
      }
    }
  }
`;

export const getTratamientos = gql`
  query {
    getTratamineto {
      idTratamiento
      nombre_tratamiento
      costo_tratamiento
    }
  }
`;

export const getIdPaciente = gql`
  query getIdPaciente($id_usuario: Int!) {
    getPacienteid(id_usuario: $id_usuario) {
      id_paciente
    }
  }
`;

export const gethistclinico = gql`
  query gethistclinico($id_paciente: Int!, $token: String!) {
    gethistclinico(id_paciente: $id_paciente, token: $token) {
      status
      id_paciente
      histoClini {
        id_paciente
        enfermedades
      }
    }
  }
`;

export const getCitaId = gql`
  query getCitaId($idPacientes: Int!) {
    getCitaId(idPacientes: $idPacientes) {
      idPacientes
      idPacientes
      nombre_tratamiento
      fecha_cita
      costo_tratamiento
      estatus_cita
    }
  }
`;

export const login = gql`
  query login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      status
      message
      token
    }
  }
`;

export const getHistorialDen = gql`
  query getHistorialDen {
    getHistorialDen {
      idPacientes
      fecha_Historial
      nombre_tratamiento
      costo_tratamiento
    }
  }
`;
