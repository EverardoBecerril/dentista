import gql from 'graphql-tag';

export const registerData = gql`
  mutation addUser($user: UserInput!) {
    registerUser(user: $user) {
      status
      message
      user {
        nombre_usuario
        apellido_usuario
        telefono
        fecha_nacimiento
        genero
        id_tipo_usuario
        email
      }
    }
  }
`;

export const setCita = gql`
mutation setCita($cita: InputCita!) {
  setCita(cita: $cita) {
    status
  }
}
`;
