-- MySQL dump 10.13  Distrib 8.0.19, for osx10.14 (x86_64)
--
-- Host: localhost    Database: dentista
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Citas`
--

DROP TABLE IF EXISTS `Citas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Citas` (
  `idCitas` int(11) NOT NULL AUTO_INCREMENT,
  `idPacientes` int(11) NOT NULL,
  `Tratamiento_idTratamiento` int(11) NOT NULL,
  `fechaUsuario_cita` text,
  `fecha_cita` datetime DEFAULT NULL,
  `estatus_cita` enum('En espera','Aprobado') NOT NULL DEFAULT 'En espera',
  PRIMARY KEY (`idCitas`),
  KEY `fk_citas_tratamiento1_idx` (`Tratamiento_idTratamiento`),
  CONSTRAINT `fk_citas_tratamiento1` FOREIGN KEY (`Tratamiento_idTratamiento`) REFERENCES `Tratamiento` (`idTratamiento`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Citas`
--

LOCK TABLES `Citas` WRITE;
/*!40000 ALTER TABLE `Citas` DISABLE KEYS */;
INSERT INTO `Citas` VALUES (1,4,1,'no importa lo que haya aca','2020-05-30 11:00:00','Aprobado'),(2,89,3,'fadfalkdfuhafa','2020-05-30 11:00:00','Aprobado'),(3,67,1,'egdgfadg','2020-06-02 09:46:45','Aprobado'),(4,56,3,'Me gustaría que mi cita fuera a tal hora','2020-06-02 09:46:45','Aprobado'),(5,26,2,'no se que va aca','2020-06-02 09:46:45','Aprobado'),(6,34,2,'quiero a las 5','2020-06-02 09:46:45','Aprobado'),(7,22,1,'Me gustaría que mi cita sea a esta hora','2020-06-27 12:24:00','Aprobado'),(9,22,1,'Me gustaría que mi cita sea a esta hora','2020-06-27 12:34:00','Aprobado');
/*!40000 ALTER TABLE `Citas` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `agregarHist` AFTER UPDATE ON `citas` FOR EACH ROW BEGIN
IF NEW.estatus_cita <> 1 THEN
  INSERT INTO Historial(fecha_Historial, Citas_idCitas) VALUES (new.fecha_cita, new.idCitas);
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Historial`
--

DROP TABLE IF EXISTS `Historial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Historial` (
  `idHistorial` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_Historial` datetime DEFAULT NULL,
  `Citas_idCitas` int(11) NOT NULL,
  PRIMARY KEY (`idHistorial`),
  KEY `fk_Historial_Citas1_idx` (`Citas_idCitas`),
  CONSTRAINT `fk_Historial_Citas1` FOREIGN KEY (`Citas_idCitas`) REFERENCES `Citas` (`idCitas`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Historial`
--

LOCK TABLES `Historial` WRITE;
/*!40000 ALTER TABLE `Historial` DISABLE KEYS */;
INSERT INTO `Historial` VALUES (1,'2020-06-13 00:00:00',1),(2,'2020-06-27 00:10:41',2),(3,'2020-05-30 11:00:00',1),(4,'2020-05-30 11:00:00',2),(5,'2020-06-02 09:46:45',3),(6,'2020-06-02 09:46:45',4),(7,'2020-06-02 09:46:45',5),(8,'2020-06-02 09:46:45',6),(9,'2020-06-27 12:24:00',7),(10,'2020-06-27 12:34:00',9);
/*!40000 ALTER TABLE `Historial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tratamiento`
--

DROP TABLE IF EXISTS `Tratamiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Tratamiento` (
  `idTratamiento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_tratamiento` varchar(45) NOT NULL,
  `costo_tratamiento` float NOT NULL,
  PRIMARY KEY (`idTratamiento`),
  CONSTRAINT `chkPrice` CHECK ((`costo_tratamiento` between 100 and 5000))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tratamiento`
--

LOCK TABLES `Tratamiento` WRITE;
/*!40000 ALTER TABLE `Tratamiento` DISABLE KEYS */;
INSERT INTO `Tratamiento` VALUES (1,'Extraccion',2500),(2,'Limpieza',100),(3,'Consulta general',200);
/*!40000 ALTER TABLE `Tratamiento` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-01 13:04:44
